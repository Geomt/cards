//
//  ViewController.m
//  cards
//
//  Created by Jorge Salgado on 03/06/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import "ViewController.h"
#import "DraggableView.h"
#import "BackgroundImageView.h"

@interface ViewController () <DraggableViewProtocol>
@property (weak, nonatomic) IBOutlet DraggableView *cardImageView;
@property (weak, nonatomic) IBOutlet BackgroundImageView *backgroundCardView;

@end

@implementation ViewController {
    NSInteger _nextIndex;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _nextIndex = 3;
    self.cardImageView.delegate = self;
    
    [self.backgroundCardView resetViewToDefaultValues];
    
}


#pragma mark - DraggableView delegate methods

- (void)isDragging:(CGFloat)currentPositionPercentage {
    [self.backgroundCardView applyEffectsWithPercentage:currentPositionPercentage];
}

- (void)draggingWillEndForView:(DraggableView *)draggableView hasPassedThreshold:(BOOL)hasPassedThreshold duration:(NSTimeInterval)duration {
    if ([draggableView isEqual:self.cardImageView]) {
        [UIView animateWithDuration:duration
                         animations:^{
                             if (hasPassedThreshold) {
                                 [self.backgroundCardView viewToFinalState];
                             }
                             else {
                                 [self.backgroundCardView resetViewToDefaultValues];
                             }
                         } completion:^(BOOL finished) {
                             if (hasPassedThreshold) {
                                 self.cardImageView.image = self.backgroundCardView.image;
                                 self.backgroundCardView.image = [self nextImage];
                                 
                                 [self.cardImageView resetViewToDefaultValues];
                             }
                         }];
    
    }
}

- (UIImage *)nextImage {
    _nextIndex++;
    NSString *imageName = [NSString stringWithFormat:@"card_%02d",_nextIndex];
    return [UIImage imageNamed:imageName];
}

@end
