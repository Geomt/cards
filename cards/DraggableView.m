//
//  DraggableView.m
//  cards
//
//  Created by Jorge Salgado on 03/06/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import "DraggableView.h"

CGFloat DegreesToRadians(const CGFloat degrees) {
    return degrees * (M_PI/180.0);
}

CGPoint CGPointAdd(const CGPoint a, const CGPoint b) {
    return CGPointMake(a.x + b.x,
                       a.y + b.y);
}

CGPoint CGPointSubtract(const CGPoint minuend, const CGPoint subtrahend) {
    return CGPointMake(minuend.x - subtrahend.x,
                       minuend.y - subtrahend.y);
}


CGRect CGRectExtendedOutOfBounds(const CGRect rect,
                                    const CGRect bounds,
                                    const CGPoint translation) {
    CGRect destination = rect;
    while (!CGRectIsNull(CGRectIntersection(bounds, destination))) {
        destination = CGRectMake(CGRectGetMinX(destination) + translation.x,
                                 CGRectGetMinY(destination) + translation.y,
                                 CGRectGetWidth(destination),
                                 CGRectGetHeight(destination));
    }
    
    return destination;
}



@interface DraggableView ()


@property(nonatomic, strong) UIImageView *overlayView;
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, assign) CGPoint referencePoint;
@property (nonatomic, assign) CGRect referenceFrame;

@end

@implementation DraggableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self commonInit];
}

- (void)commonInit {
    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragView:)];
    [self addGestureRecognizer:self.panGestureRecognizer];
    
    self.overlayView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 290, 255)];
    self.overlayView.alpha = 0;
    [self addSubview:self.overlayView];
    
    _maxRotationAngle = 20.f;
    _threshold = 120.f;
    _minScaleValue = 0.8f;
    _cancelAnimationDuration = 0.2f;
    _completeAnimationDuration = 0.1f;
}


- (void)dragView:(UIPanGestureRecognizer *)gestureRecognizer
{
    CGPoint translationPoint = [gestureRecognizer translationInView:self];
    
    switch (gestureRecognizer.state) {
        case UIGestureRecognizerStateBegan:{
            self.referencePoint = self.center;
            self.referenceFrame = self.frame;
            break;
        };
        case UIGestureRecognizerStateChanged:{
            CGFloat translationSign = translationPoint.x >= 0 ? 1.f : -1.f;
            CGFloat xTranslation = MIN(fabs(translationPoint.x),self.threshold);
            CGFloat currentPositionPercentage = xTranslation/self.threshold;
            
            CGFloat rotation = DegreesToRadians(currentPositionPercentage * self.maxRotationAngle);
            CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity,rotation * translationSign);
            
            CGFloat scale = 1 -  ((1 - self.minScaleValue) * currentPositionPercentage);
            CGAffineTransform scaleTransform = CGAffineTransformScale(transform, scale, scale);
            self.transform = scaleTransform;
            
            
            self.center = CGPointAdd(self.referencePoint,translationPoint);
            
            [self updateOverlay:translationSign percentage:currentPositionPercentage];
            
            [self.delegate isDragging:currentPositionPercentage];
            
            break;
        };
        case UIGestureRecognizerStateEnded: {
            if ([self hasPassedThreshold]) {
                [self moveOutOfBounds];
            }
            else {
                [self cancelMovement];
            }
            

            break;
        };
        case UIGestureRecognizerStatePossible:break;
        case UIGestureRecognizerStateCancelled:break;
        case UIGestureRecognizerStateFailed:break;
    }
}

- (void)updateOverlay:(CGFloat)translationSign percentage:(CGFloat)percentage{
    if (translationSign == 0.0f) {
        self.overlayView.image = nil;
    }
    else {
        if (translationSign > 0) {
            self.overlayView.image = [UIImage imageNamed:@"visual_clue_yes"];
        }
        else {
            self.overlayView.image = [UIImage imageNamed:@"visual_clue_no"];
        }
        self.overlayView.alpha = percentage;
    }

}



- (BOOL)hasPassedThreshold {
    if (fabs(self.center.x - self.referencePoint.x) >  self.threshold) {
        return YES;
    }
    return NO;
}

- (void)moveOutOfBounds {
    if ([self.delegate respondsToSelector:@selector(draggingWillEndForView:hasPassedThreshold:duration:)]) {
        [self.delegate draggingWillEndForView:self hasPassedThreshold:YES duration:self.completeAnimationDuration];
    }
    
    CGPoint translation = CGPointSubtract(self.center,
                                             self.referencePoint);
    CGRect destination = CGRectExtendedOutOfBounds(self.frame,
                                                      self.superview.bounds,
                                                      translation);
    [UIView animateWithDuration:self.completeAnimationDuration
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.frame = destination;
                     } completion:nil];
}


- (void)resetViewToDefaultValues {
    self.center = self.referencePoint;
    self.transform = CGAffineTransformMakeRotation(0);
    self.frame = self.referenceFrame;
    self.overlayView.alpha = 0;
}

- (void)cancelMovement {
    if ([self.delegate respondsToSelector:@selector(draggingWillEndForView:hasPassedThreshold:duration:)]) {
        [self.delegate draggingWillEndForView:self hasPassedThreshold:NO duration:self.cancelAnimationDuration];
    }
    [UIView animateWithDuration:self.cancelAnimationDuration
                     animations:^{
                         [self resetViewToDefaultValues];
                     }];
}



@end
