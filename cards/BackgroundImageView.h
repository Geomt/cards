//
//  BackgroundImageView.h
//  cards
//
//  Created by Jorge Salgado on 03/06/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackgroundImageView : UIImageView

@property (nonatomic, assign) CGFloat minScaleFactor;
@property (nonatomic, assign) CGFloat overThresholdScaleFactor;
@property (nonatomic, assign) CGFloat minAlpha;

- (void)resetViewToDefaultValues;
- (void)applyEffectsWithPercentage:(CGFloat)percentage;
- (void)viewToFinalState;

@end
