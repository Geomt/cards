//
//  DraggableView.h
//  cards
//
//  Created by Jorge Salgado on 03/06/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DraggableView;

@protocol DraggableViewProtocol <NSObject>

@required

- (void)isDragging:(CGFloat)currentPositionPercentage;

@optional
- (void)draggingWillEndForView:(DraggableView *)draggableView hasPassedThreshold:(BOOL)hasPassedThreshold duration:(NSTimeInterval)duration;

@end

@interface DraggableView : UIImageView

@property (nonatomic, assign) CGFloat threshold;
@property (nonatomic, assign) CGFloat maxRotationAngle;
@property (nonatomic, assign) CGFloat minScaleValue;
@property (nonatomic, assign) CGFloat cancelAnimationDuration;
@property (nonatomic, assign) CGFloat completeAnimationDuration;


@property (nonatomic, weak) id<DraggableViewProtocol> delegate;

- (void)resetViewToDefaultValues;


@end
