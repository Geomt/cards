//
//  AppDelegate.h
//  cards
//
//  Created by Jorge Salgado on 03/06/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
