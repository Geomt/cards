//
//  BackgroundImageView.m
//  cards
//
//  Created by Jorge Salgado on 03/06/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import "BackgroundImageView.h"

@implementation BackgroundImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self commonInit];
}

- (void)commonInit {
    
    _minScaleFactor = 0.75f;
    _overThresholdScaleFactor = 0.9f;
    _minAlpha = 0.4f;
    
}

- (void)resetViewToDefaultValues {
    self.transform = CGAffineTransformMakeScale(self.minScaleFactor, self.minScaleFactor);
    self.alpha = self.minAlpha;
}

- (void)applyEffectsWithPercentage:(CGFloat)percentage {
    CGFloat scale =  self.minScaleFactor + (self.overThresholdScaleFactor - self.minScaleFactor) * percentage;
    self.transform = CGAffineTransformMakeScale(scale, scale);
    self.alpha = self.minAlpha + (1.0 - self.minAlpha) * percentage;
}

- (void)viewToFinalState {
    self.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.alpha = 1.0;
}

@end
